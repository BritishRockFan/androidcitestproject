package com.example.androidcitestproject;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class ButtonsTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void defaultMessage() {
        onView(withId(R.id.text_field)).check(matches(withText("Здесь появится текст")));
    }

    @Test
    public void firstButtonCheck() {
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.text_field)).check(matches(withText("Лол!")));
    }

    @Test
    public void secondButtonCheck() {
        onView(withId(R.id.button2)).perform(click());
        onView(withId(R.id.text_field)).check(matches(withText("Делай")));
    }

    @Test
    public void thirdButtonCheck() {
        onView(withId(R.id.button3)).perform(click());
        onView(withId(R.id.text_field)).check(matches(withText("Тесты!!!")));
    }
}
